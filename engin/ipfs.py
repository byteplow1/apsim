class IpfsNetwork:
    def __init__(self):
        self.dth = {}

    def createPeer(self):
        return IpfsPeer(self)

class IpfsPeer:
    def __init__(self, network):
        self.network = network
        self.data = {}
        self.reachable = True # not the same as Instace.reachable
        self.instaceIdStr = 'unknown'

    def get(self, cid):
        if cid not in self.network.dth or self.network.dth[cid] == []:
            print('instace {} failed to retrive {}'.format(self.instaceIdStr, cid))
            return None

        for peer in self.network.dth[cid]:
            if not peer.reachable:
                continue

            if cid not in peer.data:
                print('instace {} failed to retrive {}'.format(self.instaceIdStr, cid))
                return None

            print('instace {} retrived {}'.format(self.instaceIdStr, cid))
            return peer.data[cid]

        print('instace {} failed to retrive {}'.format(self.instaceIdStr, cid))
        return None

    def pin(self, obj):
        if type(obj) is Actor or type(obj) is Collection:
            cid = obj.cid
        else:
            cid = obj['id']

        print('instace {} pinned {}'.format(self.instaceIdStr, cid))

        if cid not in self.network.dth:
            self.network.dth[cid] = []

        if self not in self.network.dth[cid]:
            self.network.dth[cid].append(self)

        self.data[cid] = obj

        if type(obj) is dict:
            if 'object' in obj and 'id' in obj['object']:
                self.pin(obj['object']) # recursively pin subobjcts

    def delete(self, cid):
        if cid not in self.network.dth:
            return

        if self not in self.network.dth[cid]:
            return

        self.network.dth[cid].remove(self)
        del self.data[cid]

class Simulation:
    def __init__(self):
        self.instaceCounter = 0
        self.IpfsNetwork = IpfsNetwork()
        self.outboxes = [] 
        self.instances = []

    def createInstance(self):
        peer = self.IpfsNetwork.createPeer()
        instance = Instance(peer, len(self.instances), self)
        self.instances.append(instance)
        return instance

    def tick(self):
        for outbox in self.outboxes:
            outbox.freezeQueue() 

        for outbox in self.outboxes:
            outbox.execQueue()

    def createOutbox(self, actor):
        outbox = Outbox(actor)
        self.outboxes.append(outbox)
        return outbox

class Instance:
    def __init__(self, ipfsPeer, instanceId, simulation):
        self.simulation = simulation
        self.instanceId = instanceId
        self.ipfsPeer = ipfsPeer
        self.ipfsPeer.instaceIdStr = str(self.instanceId)
        self.objectCounter = 0
        self.reachable = True # not the same as IpfsPeer.reachable
        self.actors

    def createActor(self):
        cid = self.nextCid()
        actor = Actor(cid, self)
        self.ipfsPeer.pin(actor)
        self.actors.append(actor)
        return actor

    def createCollection(self)
        cid = self.nextCid()
        collection = Collection(cid)
        self.ipfsPeer.pin(collection)

        return collection

    def nextCid(self):
        cid = str(self.instanceId) + '/' + str(self.objectCounter)
        self.objectCounter += 1
        return cid

class Actor:
    def __init__(self, cid, instace):
        self.cid = cid
        self.instace = instace
        self.inbox = Inbox(self)
        self.outbox = self.instace.simulation.createOutbox(self)
        self.followers = self.instance.createCollection()
        self.followees = self.instance.createCollection()

class Inbox:
    def __init__(self, actor):
        self.actor = actor
        self.log = []

    def post(self, activity):
        if not self.actor.instace.reachable:
            raise Exception("Instace not reachable")

        # introduce pinning stragety for actvities
        self.actor.instace.ipfsPeer.pin(activity)

        self.log.append(activity['id'])
        self._sideeffects(activity)

    def _sideeffects(self, activity):
        if activity['type'] == 'ping':
            self.actor.outbox.post({
                "type": "pong",
                "actor": self.actor.cid,
                "audience": [
                    activity['actor'],
                ],
                "object": activity["object"]
            })

class Outbox:
    def __init__(self, actor):
        self.actor = actor
        self.queue = []
        self.log = []

    def post(self, activity):
        self.queue.append(activity)

    def _post(self, activity):
        if 'object' in activity and 'id' not in activity['object']:
            cid = self.actor.instace.nextCid()
            activity['object']['id'] = cid

        cid = self.actor.instace.nextCid()
        activity['id'] = cid
        self.actor.instace.ipfsPeer.pin(activity)

        self.log.append(cid)

        self._sideeffects(activity)

        self._deliver(activity)

    def _deliver(self, activity, audience=None):
        if audience is None:
            audience = activity['audience']

        for x in audience:
            if type(x) is str:
                x = self.actor.instace.ipfsPeer.get(x)

            if type(x) is Actor:
                self.actor.instace.ipfsPeer.pin(x)

                x.inbox.post(activity)
                continue

            if type(x) is Collection:
                self.actor.instace.ipfsPeer.ping(x)

                self._deliver(activity, x.items)
                continue

            raise Exception("Not implemented")

    def _sideeffects(self, activity):
        pass

    def freezeQueue(self):
        self.frozenQueue = self.queue
        self.queue = []

    def execQueue(self):
        for activity in self.frozenQueue:
            self._post(activity)

class Collection:
    def __init__(self, cid):
        self.cid = cid
        self.items = []