import copy

class Network:
    def __init__(self):
        self.objectTable = {}
        self.nameTable = {}
        self.cidCounter = 0

    def createPeer(self):
        return Peer(self)

class Peer:
    def __init__(self, network):
        self.objectStore = {}
        self.network = network
        self.active = True
        self.namePermission = {}

    def get(self, uri):
        originalUri = uri
        if uri.startswith('ipns/'):
            uri = self.network.nameTable[uri]

        peers = self.network.objectTable[uri]

        for peer in peers:
            if not peer.active:
                continue

            if uri not in peer.objectStore:
                continue

            obj = copy.deepcopy(peer.objectStore[uri])

            if type(obj) is dict:
                obj['id'] = originalUri
            else:
                obj.uri = originalUri

            return obj

        raise Exception('object not found: {}'.format(uri))

    def pin(self, obj):
        if type(obj) is dict:
            uri = obj['id']
        else:
            uri = obj.uri

        if uri.startswith('ipns/'):
            uri = self.network.nameTable[uri]
        
        if uri not in self.network.objectTable:
            self.network.objectTable[uri] = []

        if self not in self.network.objectTable[uri]:
            self.network.objectTable[uri].append(self)

        self.objectStore[uri] = copy.deepcopy(obj)

    def create(self, obj):
        uri = 'ipfs/' + str(self.network.cidCounter)
        self.network.cidCounter += 1

        if type(obj) is dict:
            obj['id'] = uri
        else:
            obj.uri = uri

        self.pin(obj)

        return uri

    def update(self, obj):
        if type(obj) is dict:
            uri = obj['id']
        else:
            uri = obj.uri

        if not uri.startswith('ipns/'):
            raise Exception('only objects with ipns id can be updated: {}'.format(uri))

        if uri not in self.namePermission or self.namePermission[uri] is not True:
            raise Exception('peer is not allowed to update object: {}'.format(uri))

        versionUri = self.create(copy.deepcopy(obj))

        self.network.nameTable[uri] = versionUri

        return uri

    def createName(self):
        uri = 'ipns/' + str(self.network.cidCounter)
        self.network.cidCounter += 1

        self.namePermission[uri] = True

        return uri

if __name__ == '__main__':
    n = Network()

    p1 = n.createPeer()
    p2 = n.createPeer()

    print('create and get')
    u1 = p1.create({
        'id': p1.createNewName(),
        'somekey': 'somevalue'
    })

    print(u1)
    print(p1.get(u1))
    print(p2.get(u1))
    

    print('create and get node down')
    u2 = p1.create({
        'somekey': 'someothervalue'
    })
    print(u2)
    p1.active = False
    try:
        print(p2.get(u2))
    except Exception as e:
        print(e)
    p1.active = True
    print(p2.get(u2))


    print('update, get, update and get')
    u3 = p1.update({
        'id': p1.createName(),
        'somekey': 'someothervalue'
    })
    print(u3)
    print(p2.get(u3))
    o1 = p1.get(u3)
    o1['somekey'] = 'anothervalue'
    p1.update(o1)
    print(p2.get(u3))