import apipfs
import copy

class Network:
    def __init__(self, apipfsNetwork):
        self.apipfsNetwork = apipfsNetwork
        self.boxCounter = 0
        self.boxTable = {}

    def createInstance(self):
        return Instance(self.apipfsNetwork.createPeer(), self)

class Instance:
    def __init__(self, peer, network):
        self.network = network
        self.peer = peer
        self.actors = []
        self.outboxes = []

    def createActor(self):
        uri = self.peer.createName()
        actor = {
            'id': uri,
            'type': 'actor',
            'followers': self.createCollection(),
            'inbox': self.createInbox(uri),
            'outbox': self.createOutbox(uri),
        }

        self.peer.update(actor)
        self.actors.append(uri)

        return uri

    def addFollower(self, actor, followee):
        if type(actor) is str:
            actor = self.peer.get(actor)

        followers = actor['followers']
    
        self.collectionAppend(followers, followee)

    def collectionAppend(self, collection, item):
        if type(collection) is str:
            collection = self.peer.get(collection)

        if not type(item) is str:
            if type(item) is dict:
                item = item['id'] 
            else:
                item = item.uri

        if item in collection['items']:
            return

        collection['items'].append(item)

        self.peer.update(collection)

    def createCollection(self):
        uri = self.peer.createName()

        collection = {
            'id': uri,
            'type': 'collection',
            'items': [],
        }

        self.peer.update(collection)

        return uri

    def getBox(self, uri):
        if not uri.startswith('box/'):
            raise Exception('box uris need to start with "box/": {}'.format(uri))

        return self.network.boxTable[uri]

    def _nextBoxUri(self):
        uri = 'box/' + str(self.network.boxCounter)
        self.network.boxCounter += 1
        return uri

    def createInbox(self, actor):
        uri = self._nextBoxUri()
        box = Inbox(uri, actor, self)
        self.network.boxTable[uri] = box
        return uri

    def createOutbox(self, actor):
        uri = self._nextBoxUri()
        box = Outbox(uri, actor, self)
        self.network.boxTable[uri] = box
        self.outboxes.append(box)
        return uri

    def _freeze(self):
        for outbox in self.outboxes:
            outbox._freezeQueue()

    def _process(self):
        for outbox in self.outboxes:
            outbox._processQueue()

class Inbox:
    def __init__(self, uri, actor, instance):
        self.uri = uri
        self.actor = actor
        self.instance = instance
        self.log = []

    def post(self, activity):
        self.instance.peer.pin(activity)

        self.log.append(activity)


class Outbox:
    def __init__(self, uri, actor, instance):
        self.uri = uri
        self.actor = actor
        self.instance = instance
        self.queue = []
        self.frozenQueue = []
        self.log = []

    def post(self, activity):
        self.queue.append(activity)

    def _post(self, activity):
        uri = self.instance.peer.create(activity)

        self.log.append(uri)

        self._deliver(activity)

    def _deliver(self, activity, audience=None):
        if audience is None:
            audience = activity['audience']

        for obj in audience:
            if type(obj) is str and ( obj.startswith('ipfs/') or obj.startswith('ipns/') ):
                obj = self.instance.peer.get(obj)
            
            if type(obj) is dict and obj['type'] == 'actor':
                inbox = self.instance.getBox(obj['inbox'])
                inbox.post(copy.deepcopy(activity))
                continue

            if type(obj) is dict and obj['type'] == 'collection':
                self._deliver(activity, obj['items'])
                continue

            raise Exception('delivery dose not know how to handle this audience type: {}'.format(str(obj)))

    def _freezeQueue(self):
        self.frozenQueue = self.queue
        self.queue = []

    def _processQueue(self):
        for activity in self.frozenQueue:
            self._post(activity)

if __name__ == '__main__':
    n = Network(apipfs.Network())
    i1 = n.createInstance()

    print('create actors')
    a1 = i1.createActor()
    print(a1)
    print(i1.peer.get(a1))

    a2 = i1.createActor()
    print(a2)
    print(i1.peer.get(a2))

    print('follow')
    i1.addFollower(a1, a2)
    print(i1.peer.get('ipns/1'))
    print(i1.peer.get('ipns/5'))

    a3 = i1.createActor()
    i1.addFollower(a1, a3)
    i1.addFollower(a1, a2)
    print(i1.peer.get('ipns/1'))

    print('send activity')
    obu1 = i1.peer.get(a1)['outbox']
    ob1 = i1.getBox(obu1)
    ob1.post({
        'type': 'create',
        'audience': [
            a2,
        ],
        'object': {
            'type': 'note',
            'content': 'some value'
        },
    })

    ibu2 = i1.peer.get(a2)['inbox']
    ib2 = i1.getBox(ibu2)
    print(ib2.log)
    i1._freeze()
    i1._process()
    print(ib2.log)

    print('send activity followers')
    f1 = i1.peer.get(a1)['followers']
    ob1.post({
        'type': 'create',
        'audience': [
            f1,
        ],
        'object': {
            'type': 'note',
            'content': 'to the followers',
        },
    })
    i1._freeze()
    i1._process()
    print(ib2.log)

    ibu3 = i1.peer.get(a3)['inbox']
    ib3 = i1.getBox(ibu3)
    print(ib3.log)
