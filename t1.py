from engin.ipfs import Simulation

s = Simulation()

def createInstances(self, number):
    for _ in range(0, number):
        s.createInstance()

def createUsers(self, userPerInstance):
    for instance in self.instances:
        for _ in range(0, userPerInstance):
            instance.createActor()